# https://projecteuler.net/problem=23
# A perfect number is a number for which the sum of its proper divisors is exactly equal to the number. For example, the sum of the proper divisors of 28 would be 1 + 2 + 4 + 7 + 14 = 28, which means that 28 is a perfect number.
#
# A number n is called deficient if the sum of its proper divisors is less than n and it is called abundant if this sum exceeds n.
#
# As 12 is the smallest abundant number, 1 + 2 + 3 + 4 + 6 = 16, the smallest number that can be written as the sum of two abundant numbers is 24. By mathematical analysis, it can be shown that all integers greater than 28123 can be written as the sum of two abundant numbers. However, this upper limit cannot be reduced any further by analysis even though it is known that the greatest number that cannot be expressed as the sum of two abundant numbers is less than this limit.
#
# Find the sum of all the positive integers which cannot be written as the sum of two abundant numbers.


MAX_INT_NOT_SUM_OF_ABUNDANT = 28123


def proper_divisors(x):
    return [i for i in range(1, (x // 2) + 1) if x % i == 0]


def abundant(x):
    return sum(proper_divisors(x)) > x


def all_abundant_ordered():
    all_abundant = []
    for x in range(MAX_INT_NOT_SUM_OF_ABUNDANT + 1):
        if abundant(x):
            all_abundant.append(x)
    return all_abundant


def can_be_written_as_sum_of_abundant():
    result = set()
    abundant = all_abundant_ordered()
    for x in abundant:
        for y in abundant:
            result.add(x + y)
    return result


def cannot_be_written_as_sum_of_abundant():
    can_be = can_be_written_as_sum_of_abundant()
    return [i for i in range(MAX_INT_NOT_SUM_OF_ABUNDANT + 1) if i not in can_be]


if __name__ == '__main__':
    print(sum(cannot_be_written_as_sum_of_abundant()))
